import { Injectable } from '@angular/core';

import {Observable} from 'rxjs/Observable';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class ForgotPasswordService {

  private url = environment.url;
 
  constructor(private http:HttpClient,) { }
  
  userForgetPassword(form){

    return this.http.post(this.url + '/reset-password',form)
    
  }

  userChangePassword(form,param){

    return this.http.post(this.url + '/new-password?qID='+param,form)
    
  }

}
