export const menu = [

    {
        title: 'Menu',
        url: '/home',
        direct: 'root',
        icon: 'cafe',
        noti: false
    },
   
    {
        title: 'Product',
        url: '/item',
        direct: 'forward',
        icon: 'ice-cream',
        noti: false
    },
    {
        title: 'My Cart',
        url: '/review-order',
        direct: 'forward',
        icon: 'cart',
        noti: true
    },
    {
        title: 'Order',
        url: '/order',
        direct: 'forward',
        icon: 'book',
        noti: false
    },
    {
        title: 'Contact',
        url: '/contact',
        direct: 'forward',
        icon: 'text',
        noti: false
    },
  

    //  {
    //   title: 'ordering',
    //   url: '/ordering',
    //   direct: 'forward',
    //   icon: 'information-circle-outline'
    // },

];

export const menu_arbic = [

    {
        title: 'القائمة',
        url: '/home',
        direct: 'root',
        icon: 'cafe',
        noti: false
    },
   
    {
        title: 'المنتج',
        url: '/item',
        direct: 'forward',
        icon: 'ice-cream',
        noti: false
    },
    {
        title: 'سلتي',
        url: '/review-order',
        direct: 'forward',
        icon: 'cart',
        noti: true
    },
    {
        title: 'الطلب',
        url: '/order',
        direct: 'forward',
        icon: 'book',
        noti: false
    },
    {
        title: 'تواصل معنا',
        url: '/contact',
        direct: 'forward',
        icon: 'text',
        noti: false
    },
  

    //  {
    //   title: 'ordering',
    //   url: '/ordering',
    //   direct: 'forward',
    //   icon: 'information-circle-outline'
    // },

];