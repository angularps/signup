import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './component/login.component';
import { AppRouting } from '../../router/router';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginService } from './service/login.service';

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  AuthService,
} from "angular-6-social-login";


export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("729977040670830")
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("304120430970-10hqcmfcn2444bg4kjc8tjdsc0m7u2a2.apps.googleusercontent.com")
      }

    ]
  );
  return config;
}


@NgModule({
  imports: [
    CommonModule,
    AppRouting,
    ReactiveFormsModule,
    SocialLoginModule

  ],
  providers:[
    LoginService,
    AuthService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }],
  declarations: [LoginComponent]
})
export class LoginModule { }
