import { Injectable, EventEmitter } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { environment } from "../../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class ChangePasswordService {
  private url = environment.url;
  public isLoggedIn : EventEmitter<any> = new EventEmitter();
  
  constructor(private http: HttpClient) { }

  userChangePassword(form) {
    return this.http.post(this.url + "/change-password", form);
  }

  
}
