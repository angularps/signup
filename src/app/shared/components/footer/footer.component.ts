import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  windowWidth: number;
  mobile: boolean = false;
  @HostListener('window:resize', ['$event'])
    onResize(event) {
      this.windowWidth = window.innerWidth;
      this.checkIfMobile();
    }
  constructor() {
    this.windowWidth = window.innerWidth;
  }

  ngOnInit() {
    this.checkIfMobile();
  }

  checkIfMobile() {
    if(this.windowWidth < 769) {
      document.body.classList.add('mobile');
      this.mobile = true;
    } else {
      document.body.classList.remove('mobile');
      this.mobile = false;
    }
  }
}
