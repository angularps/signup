import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from "@angular/forms"; // form validation
import { ToastsManager, Toast } from 'ng2-toastr';
import { LoginService } from '../service/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';
import { LoaderService } from '../../../shared/services/loader.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public userType: string;
  constructor(
    private titleService: Title,
    private loginService: LoginService,
    public toastr: ToastsManager,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private socialAuthService: AuthService,
    private loaderService: LoaderService,
    private location: Location

  ) {

    if (localStorage.getItem('token')){
      this.router.navigate(['/']);
    }
    this.loginForm = this.fb.group({
      email: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.pattern(
            "^[_a-z0-9-\\+]+(\\.[_a-z0-9-]+)*@" +
            "[a-z0-9-]+(\\.[a-z0-9]+)*(\\.[a-z]{2,})$"
          )
        ]
      ],
      password: ["", Validators.required],
      type: ["", Validators.required]
    })
    this.route.params.subscribe(
      param => {
        this.userType = param.type;
        this.loginForm.controls['type'].setValue(param.type);

      })

  }

  ngOnInit() {
    this.titleService.setTitle(`Sign In | title`);


  }

  /**
   *Login service call 
   */

  loginFormsubmit(form) {
    localStorage.setItem('userType', this.userType);
    this.loaderService.display(true);
    this.loginService.userLogin(form).subscribe(
      res => {
        console.log(res['token']);
        let token = res['token'];
        if (!token) {
          return this.toastr.warning('Unknown error! Please try again.');
        }
        this.loginCompleted(token);
      },
      error => {
        console.log(error);
        this.loaderService.display(false);
        this.toastr.error(error['error'].error);

      }
    )
  }

  private loginCompleted(token: any) {
    localStorage.setItem('userType', this.userType);
    //this.loaderService.display(false);
    this.toastr.success('Successfully Logged In!', 'Success!', {dismiss: 'controlled'}).then((toast: Toast)=> {
      setTimeout(() => {
        this.toastr.dismissToast(toast);
      }, 1000);
    });
    localStorage.setItem('token', token);
    const url = localStorage.getItem('redirectUrl');

    if (this.userType === 'CHEF') {
      this.router.navigate(['/profile']);
    } else if (this.userType === 'USER' && !url) {
      this.router.navigate(['/']);
    }
    else if (this.userType === 'USER' && url) {
      this.router.navigate([url]);
    } else {
      this.router.navigate(['/']);
    }
    
    this.loginService.isLoggedIn.emit(true);
  }


  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData: any) => {
        console.log(socialPlatform + " sign in data : ", (userData));
        if (socialPlatform == 'facebook') {
          this.loaderService.display(true);
          this.loginService.facebookLogin({
            'access_token': userData.token,
            'type': this.userType
          }).subscribe(
            res => {
              console.log(res);
              let token = res['token'];
              if (!token) {
                return this.toastr.warning('Unknown error! Please try again.');
              }
              this.loginCompleted(token);
            },
            error => {
              this.loaderService.display(false);
              console.log(error);
              this.toastr.error(error['error'].error)
            }
          )
        } else {
          this.loaderService.display(true);
          this.loginService.googleLogin({
            'access_token': userData.token,
            'type': this.userType
          }).subscribe(
            res => {
              console.log(res);
              let token = res['token'];
              if (!token) {
                return this.toastr.warning('Unknown error! Please try again.');
              }
              this.loginCompleted(token);
            },
            error => {
              this.loaderService.display(false);
              console.log(error);
              this.toastr.error(error['error'].error)
            }
          )
        }

      }
    );
  }

}
