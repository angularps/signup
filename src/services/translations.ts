export const TRANSLATIONS = [
  {
    en: 'Email Address',
    ar: 'البريد الالكتروني '
  },
  {
    en: 'Password',
    ar: 'كلمة السر '
  },
  {
    en: 'Sign In',
    ar: 'تسجيل الدخول'
  },
  {
    en: 'New here?',
    ar: 'مستخدم جديد'
  },
  {
    en: 'Sign Up',
    ar: 'التسجيل'
  },
  {
    en: 'Forgot password',
    ar: 'نسيت كلمة السر '
  },
  {
    en: 'Full Name',
    ar: 'الاسم بالكامل '
  }, {
    en: 'Phone Number',
    ar: 'رقم الهاتف '
  },
  {
    en: 'Confirm Password',
    ar: 'تاكيد كلمة السر '
  },
  {
    en: 'Sign Up',
    ar: 'التسجيل '
  },
  {
    en: 'Already Have an Accound?',
    ar: 'هل لديك حساب؟ تسجيل الدخول '
  },
  {
    en: 'Email format should be joe@abc.com',
    ar: 'يجب أن يكون تنسيق البريد الإلكتروني joe@abc.com'
  },
  {
    en: 'Enter Phone Numbers with Country Code',
    ar: 'أدخل أرقام الهاتف مع رمز البل'
  },
  {
    en: 'Password should contain at least a Symbol, Upper and Lower Case letters, a Digit with a minimum length of 8',
    ar: 'يجب أن تحتوي كلمة المرور على أحرف Symbol و Upper و Lower Case على الأقل ، ورقم بطول لا يقل عن 8'
  },
  {
    en: 'Password does not match',
    ar: 'كلمة السر غير متطابق'
  },

  {
    en: 'Forgot your password',
    ar: 'نسيت كلمة السر'
  }, {
    en: 'Continue',
    ar: 'استمر'
  },
  {
    en: 'Please provide a valid OTP',
    ar: 'يرجى تقديم OTP صالح'
  },
  {
    en: 'Verify',
    ar: 'التحقق'
  },
  {
    en: 'Re-generate OTP',
    ar: 'إعادة توليد OTP'
  }, {
    en: 'Sit back & Relax!',
    ar: 'استرخ واسترخ!'
  },
  {
    en: 'while verify your',
    ar: 'في حين تحقق الخاص ب'
  },
  // Item page
  {
    en: 'Menu',
    ar: 'القائمة'
  },
  {
    en: 'Fresh cool coffee',
    ar: 'قهوة طازجة باردة'
  },
  {
    en: 'Explore Our Products',
    ar: 'تعرف على منتجاتنا'
  },
  {
    en: 'Chocolate',
    ar: 'شوكولاتة'
  },
  {
    en: 'Lorem ipsum dolor',
    ar: 'منتجات أخرى في القائمة'
  },
  {
    en: 'Review Order',
    ar: 'مراجعة الطلب'
  },
  {
    en: 'Item Details',
    ar: 'تفاصيل الطلب ( المشتريات )'
  },
  {
    en: 'Camellia Special Breakfast (3 Pcs)',
    ar: 'إفطار كاميليا المميز ( 3 قطع ) ب'
  },
  {
    en: 'SAR 25:00',
    ar: '15 ريال فقط '
  },
  {
    en: 'Payment Details',
    ar: 'تفاصيل الدفع'
  },
  {
    en: 'Vat and Service Tax',
    ar: 'ضريبة القيمة المضافة والخدمة'
  },
  {
    en: 'SAR 15:00',
    ar: '15 ريال'
  },
  {
    en: 'Total Bill Amount',
    ar: 'اجمالي الفاتورة'
  },
  {
    en: 'SAR 75:00',
    ar: '75 ريال'
  },
  {
    en: 'Delivery Charges',
    ar: 'مصاريف التوصيل'
  },
  {
    en: 'SAR 0:00',
    ar: 'صفر'
  },
  {
    en: 'Place Order',
    ar: 'نفذ الطلب'
  },
  {
    en: 'Strawberry Ice Cream',
    ar: 'ايس كريم الفروالة'
  },
  {
    en: 'You’ve liked similar items before',
    ar: 'أنواع معينة أعجبتك من قبل'
  },
  {
    en: 'Our Products',
    ar: 'منتجائتنا'
  },
  {
    en: 'Home',
    ar: 'الرئيسية'
  },
  {
    en: 'Quick Bites',
    ar: 'لقيمات سريعة'
  },
  {
    en: 'Breakfast',
    ar: 'الافطار'
  },
  {
    en: 'Dessert',
    ar: 'تحلية'
  },
  {
    en: 'Log Out',
    ar: 'الخروج '
  },
  {
    en: 'Language',
    ar: 'لغة'
  },

  {
    en: '2019 Camellia. All Rights Reserved.',
    ar: '2019 كاميليا جميع الحقوق محفوظ'
  },
  // order page
  {
    en: 'Orders',
    ar: 'صفحة الطلب'
  },
  {
    en: '09 march 2019',
    ar: '9 مارس 2019'
  },

  {
    en: 'Order',
    ar: 'اطلب'
  },
  {
    en: 'Delivered',
    ar: 'تم التوصيل'
  },
  {
    en: 'Black Coffee',
    ar: 'قهوة  - العدد'
  },
  {
    en: 'SAR 5.00',
    ar: '5 ريال'
  },
  {
    en: 'Breakfast cold coffee',
    ar: 'قهوة افطار باردة - العدد'
  },
  {
    en: 'SAR 15.00',
    ar: '15 ريال'
  },
  {
    en: 'Chocolate Ice cream',
    ar: 'أيس كريم شوكولاتة - العدد'
  },
  {
    en: 'SAR 8.00',
    ar: '8 ريال'
  },
  {
    en: 'Elasponi Coffee',
    ar: 'قهوة الابسوني - العدد'
  },
  {
    en: 'Repeat Order',
    ar: 'مراجعة الطلب'
  },
  {
    en: 'Total',
    ar: 'الإجمالي'
  },
  {
    en: 'SAR 33.00',
    ar: '33 ريال'
  },

  {
    en: '',
    ar: ''
  },
  {
    en: '',
    ar: ''
  },
  {
    en: '',
    ar: ''
  },
  {
    en: '',
    ar: ''
  },
  {
    en: '',
    ar: ''
  },


  {
    en: '',
    ar: ''
  },
  {
    en: '',
    ar: ''
  },
  {
    en: '',
    ar: ''
  },

  {
    en: '',
    ar: ''
  },
  {
    en: '',
    ar: ''
  }, {
    en: '',
    ar: ''
  },


]