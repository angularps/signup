
import { environment } from '../environments/environment'
const mainUrl = environment.mainUrl

export const API = {
  logIn: mainUrl + 'auth/login',
  signup: mainUrl + 'auth/signup',
  otp: mainUrl + 'auth/verifyotp',
  resent: mainUrl + 'auth/resendotp',
  forgotPassword: mainUrl + "auth/forgotPassword",
  resPassword: mainUrl + "auth/resetPassword",
  getCatgeory: mainUrl + "categories?filter=all",
  updateProfile: mainUrl + 'customer',
  refresh_token: mainUrl + 'refresh-token',
  getPaymeentOrderID: mainUrl+ 'confirm-booking',
  successPaymentURl:mainUrl+'capture-payment',
  getOrderDetails: mainUrl + 'customer-order-details'

}
export let token = {
  Token: undefined
};


export const activeClass ={
  active:''
}

