import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../../pages/login/service/login.service';
import { ProfileService } from '../../../pages/profile/service/profile.service';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [ProfileService]
})
export class HeaderComponent implements OnInit {

  public setPassword: string = "Change Password";
  public isLoggedIn: boolean = false;
  userProfile: any;
  userType: string;
  windowWidth: number;
  mobile: boolean = false;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.windowWidth = window.innerWidth;
    this.checkIfMobile();
  }
  constructor(public router: Router,
    private loginService: LoginService,
    public toastr: ToastsManager,
    public profileService: ProfileService) {
    this.windowWidth = window.innerWidth;
    this.loginService.isLoggedIn.subscribe((isLoggedIn) => {
      this.isLoggedIn = isLoggedIn || false;
      this.userType = localStorage.getItem('userType');
      this.isLoggedIn ? this.getUserData() : null;
    });
    this.profileService.userProfile.subscribe((userProfile) => {
      this.userProfile = userProfile;
    });
  }

  ngOnInit() {
    const token = localStorage.getItem('token');
    this.userType = localStorage.getItem('userType');
    this.checkIfMobile();
    if (token) {
      this.isLoggedIn = true;
      this.getUserData();
    }
  }

  getUserData(): any {
    this.profileService.userProfileData().subscribe((data) => {
      if (data['data']) {
        console.log('++',data['data']);
        
        this.profileService.userProfile.emit(data['data']);
        data['data'].resetPass ? this.setPassword = 'Set Password' : this.setPassword = 'Change Password'
        data['data'].setPassword ? this.setPassword = 'Set Password' : this.setPassword = 'Change Password'

      }
    });
    if (this.userType.toUpperCase() === 'CHEF') {
      this.profileService.getUserData().subscribe((data) => {

        if (data['data'] && data['data'].user) {
          this.profileService.isProfileComplete = true;
        } else {
          this.profileService.isProfileComplete = false;
        }
        console.log(this.profileService.isProfileComplete);

      });
    }


  }
  checkIfMobile() {
    if (this.windowWidth < 769) {
      document.body.classList.add('mobile');
      this.mobile = true;
    } else {
      document.body.classList.remove('mobile');
      this.mobile = false;
    }
  }
  logout() {
    this.setPassword = 'Change Password';
    const userType = localStorage.getItem('userType');
    localStorage.clear();
    localStorage.setItem('userType', userType);
    this.loginService.isLoggedIn.emit(false);
    this.profileService.userProfile.emit(null);
    this.router.navigate([`/auth/login/${userType}`]);
  }


}
