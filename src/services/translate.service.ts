import { Injectable } from '@angular/core';
import { TRANSLATIONS } from './translations'
import { Storage } from '@ionic/storage';

@Injectable()
export class TranslateService {

  public currentLanguage: any;


  constructor(private storage: Storage) {
    this.currentLanguage = this.getLang();
  }

  translate(str) {
    return TRANSLATIONS.filter(entrada => entrada['en'] === str)[0][this.currentLanguage];
  }

  selectLanguage(language) {
    this.currentLanguage = language;
  }

  async getLang() {

    return await this.storage.get('lang');

  }

}