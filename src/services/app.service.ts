import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { HttpClient } from "@angular/common/http";
import { ToastController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class AppService {
  public userData: EventEmitter<any> = new EventEmitter();
  @Output() data: EventEmitter<boolean> = new EventEmitter();
  @Output() languageClass: EventEmitter<string> = new EventEmitter();

  currentlatitude: any;


  constructor(private http: HttpClient,
    private storage: Storage) {
  }


  post(url: string, body: any) {

    return this.http.post(url, body);
  }

  get(url: string, params) {

    const param = new HttpParams()
      .set('latitude', params.latitude)
      .set('latitude', params.longitude);

    return this.http.get(url, { params }
    )
  }

  delete() {

  }

  put(url: string, body: any) {

    return this.http.post(url, body);
  }
}


@Injectable({
  providedIn: 'root'
})
export class ToastrService {



  constructor(public toastController: ToastController) {
  }

  async show(message, timeout?, position?, showCloseButton = false) {
    const toast = await this.toastController.create({
      message: message,
      duration: timeout ? timeout : 3000,
      position: position ? position : "top",
      showCloseButton: showCloseButton
    });
    toast.present();
  }

}

@Injectable({
  providedIn: 'root'
})
export class AppLoader {
  routeURL: string;
  constructor(public loadingController: LoadingController,
  ) {
    this.routeURL = ''
  }

  async presentLoadingWithOptions(data) {
    const loading = await this.loadingController.create({
      duration: data,
      spinner: "dots",
      // message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading',
    });
    return await loading.present();
  }
}