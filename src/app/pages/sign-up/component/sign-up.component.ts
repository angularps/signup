import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators, AbstractControl } from "@angular/forms"; // form validation
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { SignUpService } from '../service/sign-up.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  userEmail: { [key: string]: AbstractControl; };
  public signupForm: FormGroup;
  public password = null;
  public confirmPassword = null;
  public passwordMatchMsg;
  public emailMatchMsg;
  public email = null;
  public confirmEmail = null;
  public signupWelcome: boolean;

  constructor(
    private titleService: Title,
    private signupService: SignUpService,
    public toastr: ToastsManager,
    private fb: FormBuilder,
    private router: Router,
    private loaderService: LoaderService,
    private location: Location

  ) {
    if (localStorage.getItem('token')) {
      this.location.back();
    }

    this.signupForm = this.fb.group({
      'userType': ['', [Validators.required]],
      'fullName': ['', [Validators.required, Validators.minLength(2)]],
      'email': ['', [Validators.required, Validators.pattern('^[_a-z0-9-\\+]+(\\.[_a-z0-9-]+)*@' + '[a-z0-9-]+(\\.[a-z0-9]+)*(\\.[a-z]{2,})$')]],
      'password': ['', [Validators.required, Validators.minLength(5), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,100})/)]],

    })
  }

  ngOnInit() {
    this.titleService.setTitle(`Sign Up | title`);

  }

  /**
   *signup service call
   */

  signUpFormsubmit() {
    this.loaderService.display(true);
    this.userEmail = this.signupForm.controls['email'].value;
    this.signupService.signUpForm(this.signupForm.value)
      .subscribe(
        res => {
          this.loaderService.display(false);
          this.signupWelcome = true;
          localStorage.setItem('userType', this.signupForm.controls['userType'].value);
          this.toastr.success(res['data'], 'Success!');

        },
        error => {
          this.loaderService.display(false);
          if (typeof error['error'].error == "string") {
            this.toastr.error(error['error'].error)
          } else if (error['error'].error.password) {
            this.toastr.error(error['error'].error.password)

          } else {
            this.toastr.error(error['error'].error.fullName)

          }

        }
      );
  }
  resendEmailAPI() {

  }

  /**
   * Signup confirm and password matching query
   */
  inputdMatch(event, type) {

    if (type == 'email') {
      this.email = event;
      if (event === '') {
        this.email = null;
      }
      if (this.confirmEmail === null) {
        // console.log('pas1')
        this.emailMatchMsg = 1;
      } else {
        if (this.confirmEmail === this.email) {
          // console.log('pas2')
          this.emailMatchMsg = 0;
        } else {
          this.emailMatchMsg = 2;
        }
      }

    } else {
      this.password = event;
      if (event === '') {
        this.password = null;
      }
      if (this.confirmPassword === null) {
        // console.log('pas1')
        this.passwordMatchMsg = 1;
      } else {
        if (this.confirmPassword === this.password) {
          // console.log('pas2')
          this.passwordMatchMsg = 0;
        } else {
          this.passwordMatchMsg = 2;
        }
      }
    }
  }

  confirmInputMatch(event, type) {
    if (type == 'email') {
      this.confirmEmail = event;
      if (event === '') {
        this.confirmEmail = null;
      }
      if (this.email === null) {
        this.emailMatchMsg = 1;
      } else {
        if (this.confirmEmail === this.email) {
          this.emailMatchMsg = 0;
        } else {
          this.emailMatchMsg = 2;
        }
      }
    } else {
      this.confirmPassword = event;
      if (event === '') {
        this.confirmPassword = null;
      }
      if (this.password === null) {
        this.passwordMatchMsg = 1;
      } else {
        if (this.confirmPassword === this.password) {
          this.passwordMatchMsg = 0;
        } else {
          this.passwordMatchMsg = 2;
        }
      }

    }

  }

}
