
import { Injectable, EventEmitter } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { environment } from "../../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class LoginService {
  private url = environment.url;
  public isLoggedIn : EventEmitter<any> = new EventEmitter();
  
  constructor(private http: HttpClient) { }

  userLogin(form) {
    return this.http.post(this.url + "/login", form);
  }

  facebookLogin(form) {
    return this.http.post(this.url + "/facebook_login", form);
  }

  googleLogin(form) {
    return this.http.post(this.url + "/google_login", form);
  }


}
