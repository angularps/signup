import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ForgotPasswordService } from '../service/forgot-password.service';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})

export class ForgotPasswordComponent implements OnInit {

  public forgotPasswordForm: FormGroup;
  public emailForm: FormGroup;
  public spinner: Boolean = false;
  public password = null;
  public confirmPassword = null;
  public passwordMatchMsg;
  public successMessage: string;
  public resetPassword: string;
  public userType: string;

  constructor(private fb: FormBuilder,
    private forgotPasswordService: ForgotPasswordService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public toastr: ToastsManager,

  ) {
    this.emailForm = this.fb.group({
      'email': ['', [Validators.required, Validators.pattern('^[_a-z0-9-\\+]+(\\.[_a-z0-9-]+)*@' + '[a-z0-9-]+(\\.[a-z0-9]+)*(\\.[a-z]{2,})$')]],
    });
    this.forgotPasswordForm = this.fb.group({
      'password': ['', [Validators.required, Validators.minLength(5), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,100})/)]],
    });

    this.activatedRoute.queryParams.subscribe(params => {
      this.resetPassword = params['qID'];
      this.userType = params['type']

    });
  }

  ngOnInit() {

  }

  /**
   *forgot password email 
   */

  emailFormsubmit(formvalue) {
    this.spinner = true;
    this.forgotPasswordService.userForgetPassword(formvalue)
      .subscribe(
        res => {
          this.spinner = false;
          this.toastr.success(res['data']);
          this.successMessage = res['data'];

        },
        error => {
          this.spinner = false;
          this.toastr.error(error['error'].error)

        }
      );
  }


  /**
   *forgot password reset password
   */

  forgotPasswordFormsubmit(formvalue) {
    this.spinner = true;
    this.forgotPasswordService.userChangePassword(formvalue, this.resetPassword)
      .subscribe(
        res => {
          this.spinner = false;
          this.toastr.success(res['data']);
          this.router.navigate(['/auth/login/' + this.userType])
        },
        error => {
          this.spinner = false;
          this.toastr.error(error['error'].error)

        }
      );
  }

  /**
   * Signup confirm and password matching query
   */
  passwordMatch(event) {
    this.password = event;
    if (event === '') {
      this.password = null;
    }
    if (this.confirmPassword === null) {
      // console.log('pas1')
      this.passwordMatchMsg = 1;
    } else {
      if (this.confirmPassword === this.password) {
        // console.log('pas2')
        this.passwordMatchMsg = 0;
      } else {
        this.passwordMatchMsg = 2;
      }
    }
  }

  ConfirmPasswordMatch(event) {
    this.confirmPassword = event;
    if (event === '') {
      this.confirmPassword = null;
    }
    if (this.password === null) {
      this.passwordMatchMsg = 1;
    } else {
      if (this.confirmPassword === this.password) {
        this.passwordMatchMsg = 0;
      } else {
        this.passwordMatchMsg = 2;
      }
    }
  }

}
