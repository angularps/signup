import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public url: any;
  constructor(private http: HttpClient) {
  }

  post(url: string, body: any) {
    return this.http.post(url, {});
  }

  get(url: string, params?: HttpParams) {
    return this.http.get(url, {
      params: params ? params : {

      }
    })
  }

  delete() {

  }

  put() {

  }
}

