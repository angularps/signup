import { Injectable } from '@angular/core';

declare var FB: any;

@Injectable()
export class SocialShareService {

  constructor() {}
  
  /**
   * 
   * @param action_properties 
   * 
   */
  FBShare(action_properties: {url: string, title: string, site_name: string, description: string, image: string}) {
    const data = action_properties;
    return new Promise((resolve, reject)=> {
      FB.ui({
        method: 'share',
        action_type: 'og.shares',
        display: 'popup',
        action_properties: JSON.stringify({
          object : {
            'og:url': data.url,
            'og:title': data.title,
            'og:site_name': data.site_name,
            'og:description': data.description,
            'og:image': data.image
          }
        })
      }, function(response){
        if (response && !response.error_message) {
          resolve(response);
        } else {
          reject('Error while posting on facebook.');
        }
      });
    })
  }


  /**
   * 
   * @param action_properties 
   */
  TwitterShare(action_properties: {url: string, text: string, hashtags: string,}):void {
    const data = action_properties;
    const shareUrl = `http://twitter.com/share?text=${data.text.slice(0, 140)}...&url=${data.url}&hashtags=${data.hashtags}`;
    window.open(shareUrl, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
    return;
  }

  
}
