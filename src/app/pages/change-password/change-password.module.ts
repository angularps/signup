import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordService } from './service/change-password.service';
import { ChangePasswordComponent } from './component/change-password.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
    
  ],
  declarations: [ChangePasswordComponent],
  providers: [ChangePasswordService]
})
export class ChangePasswordModule { }
