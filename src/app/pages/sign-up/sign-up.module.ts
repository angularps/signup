import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpComponent } from './component/sign-up.component';
import { AppRouting } from '../../router/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SignUpService } from './service/sign-up.service';

@NgModule({
  imports: [
    CommonModule,
    AppRouting,
    ReactiveFormsModule,

  ],
  providers:[SignUpService],
  declarations: [SignUpComponent]
})
export class SignUpModule { }
