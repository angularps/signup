import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { environment } from "../../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class SignUpService {

  private url = environment.url;
  constructor(private http: HttpClient) { }

  signUpForm(form) {
    return this.http.post(this.url + "/signup", form);
  }


}
