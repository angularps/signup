import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordComponent } from './component/forgot-password.component';
import { AppRouting } from '../../router/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordService } from './service/forgot-password.service';

@NgModule({
  imports: [
    CommonModule,
    AppRouting,
    ReactiveFormsModule,

  ],
  declarations: [ForgotPasswordComponent],
  providers:[ForgotPasswordService]
})
export class ForgotPasswordModule { }
