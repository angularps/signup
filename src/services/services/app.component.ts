import { Component } from '@angular/core';
import { LoaderService } from '../components/shared/services/loader.service';
import { Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { API } from '../services/constant';
import { AppService } from '../services/app.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'modwe-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  url: string;
  title = 'modwe';
  public loader: boolean = false;
  public showLoader = false;
  public LoaderState: boolean = false;
  private subscription: Subscription;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  public modalData:string = 'app';

  constructor(
    private loaderService: LoaderService,
    public snackBar: MatSnackBar,
    private service: AppService,

  ) {

    this.url = API.logIn;
    this.loaderService.status.subscribe((isLoading: boolean) => {
      this.loader = isLoading;
    });

    this.subscription = this.loaderService.loaderState
      .subscribe((isLoading: any) => {
        this.showLoader = isLoading.show;
      });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
      horizontalPosition: this.horizontalPosition,
      panelClass: 'blue-snackbar',
      verticalPosition: this.verticalPosition
    });
  }

  closeLoader() {
    this.loader = false;
  }


  login() {
    const params = new HttpParams().set('page', "1").set('limit', "1");
    console.log(params);
    this.service.get(this.url, params).subscribe(
      res => {
      })
    this.service.post(this.url, '').subscribe(
      res => {
      })
  }
}
