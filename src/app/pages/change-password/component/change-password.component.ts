import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from "@angular/forms"; // form validation
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { ChangePasswordService } from '../service/change-password.service';
import { ProfileService } from '../../profile/service/profile.service';
import { LoginService } from '../../login/service/login.service';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  setPassword: any;
  spinner: boolean = false;
  public ChangePasswordForm: FormGroup;
  public password = null;
  public confirmPassword = null;
  public passwordMatchMsg;
  public oldPassword: boolean = false
  constructor(
    private titleService: Title,
    fb: FormBuilder,
    public toastr: ToastsManager,
    private router: Router,
    private loaderService: LoaderService,
    private profileService: ProfileService,
    private loginService: LoginService,
    private service: ChangePasswordService

  ) {
    this.loaderService.display(true);
    this.ChangePasswordForm = fb.group({
      'oldPassword': [''],
      'newPassword': ['', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')]],
      'confirm_password': ['', Validators.required],

    });
  }



  ngOnInit() {
    this.titleService.setTitle(`Change password | title`);
    this.getUserData();
  }



  getUserData(): any {
    this.profileService.userProfileData().subscribe((data) => {
      if (data['data']) {
        this.loaderService.display(false);
        this.setPassword = data['data'].setPassword;

      }
    }, error => {
      this.loaderService.display(false);

    });
  }

  /**
   * user chnage  password
   */
  public UserChangePassword(form) {
    if (this.ChangePasswordForm.controls['oldPassword'].value !== '' || this.setPassword == true) {
      this.loaderService.display(true);
      this.service.userChangePassword(form)
        .subscribe(
          res => {
            this.loaderService.display(false);
            this.toastr.success(res['data']);
            this.router.navigate(['/']);
            const userType = localStorage.getItem('userType');
            localStorage.clear();
            localStorage.setItem('userType', userType);
            this.loginService.isLoggedIn.emit(false);
            this.profileService.userProfile.emit(null);
          },
          error => {
            console.log(error);
            this.toastr.error(error['error'].error);
            this.loaderService.display(false);

          }
        );
    } else {
      this.toastr.error('Old Password Required !');
    }
  }




  /**
   * password matching query
   */
  passwordMatch(event) {
    this.password = event;
    if (event === '') {
      this.password = null;
    }
    if (this.confirmPassword === null) {
      // console.log('pas1')
      this.passwordMatchMsg = 1;
    } else {
      if (this.confirmPassword === this.password) {
        // console.log('pas2')
        this.passwordMatchMsg = 0;
      } else {
        this.passwordMatchMsg = 2;
      }
    }
  }
  /**
   * Confirm password matching query
   */

  ConfirmPasswordMatch(event) {
    this.confirmPassword = event;
    if (event === '') {
      this.confirmPassword = null;
    }
    if (this.password === null) {
      this.passwordMatchMsg = 1;
    } else {
      if (this.confirmPassword === this.password) {
        this.passwordMatchMsg = 0;
      } else {
        this.passwordMatchMsg = 2;
      }
    }
  }
}
